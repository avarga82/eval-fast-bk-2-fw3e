using System;
using FluentAssertions;
using FortCodeExercises.Exercise1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FortCodeExercises.Tests
{
    
    [TestClass]
    public class MachineTests
    {
        
        [DataTestMethod]
        [DataRow(0, "bulldozer", "red", null, 80, " red bulldozer [80].")]
        [DataRow(1, "crane", "blue", "white", 75, " blue crane [75].")]
        [DataRow(2, "tractor", "green", "gold", 90, " green tractor [90].")]
        [DataRow(3, "truck", "yellow", null, 70, " yellow truck [70].")]
        [DataRow(4, "car", "brown", null, 70, " brown car [70].")]
        public void Machine_ShouldHaveCorrectValues_WhenMachineTypeFound(MachineType type, string machineName, string color, string trimColor, int maxSpeed, string description)
        {
            var machine = MachineFactory.GetMachine(type);

            machine.Should().NotBeNull();
            type.Should().Be(machine.Type);
            machineName.Should().Be(machine.Name);
            color.Should().Be(machine.Color.Name);
            trimColor.Should().Be(machine.TrimColor?.Name);
            maxSpeed.Should().Be(machine.MaxSpeed);
            description.Should().Be(machine.Description);
        }
        
        [DataTestMethod]
        [DataRow(MachineType.Bulldozer, 0)]
        [DataRow(MachineType.Crane, 1)]
        [DataRow(MachineType.Tractor, 2)]
        [DataRow(MachineType.Truck, 3)]
        [DataRow(MachineType.Car, 4)]
        public void MachineType_ShouldHaveCorrectValues(MachineType type, int expectedValue)
        {
            var machine = MachineFactory.GetMachine(type);

            machine.Should().NotBeNull();
            type.Should().Be(machine.Type);
            expectedValue.Should().Be((int)type);
        }
        
        [TestMethod]
        public void MachineFactory_ShouldThrowException_WhenMachineNotFound()
        {
            Action act = () => { MachineFactory.GetMachine(MachineType.Boat); };

            act.Should().Throw<ArgumentOutOfRangeException>();
        }
    }
}