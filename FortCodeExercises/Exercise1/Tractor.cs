namespace FortCodeExercises.Exercise1
{
    public class Tractor : Machine
    {
        public override MachineType Type => MachineType.Tractor;
        public override MachineColor Color => MachineColor.Green;
        public override MachineColor TrimColor => Color.IsDark ? MachineColor.Gold: null;
        public override int MaxSpeed => 90;
    }
}