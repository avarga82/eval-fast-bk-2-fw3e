# FORT Robotics Technical Assessment

## Summary
The existing ```Machine.cs``` class had numerous issues and needed to be refactored. Existing issues included:

 - Multiple machine types were defined in a single class and their behaviors and attributes were also defined in the same class. Each machine is unique with a distinct set of behaviors which should be encapsulated in separate classes.
 - Multiple examples of unnecessary logic. For example, the logic in the existing ```trimColor``` property defines a ```baseColor``` variable that ends up being the same exact value as the existing ```color``` property.
 - Duplicate code (ex. repeated color names).
 - Violation of SOLID principles. For example, the ```isDark()``` method has nothing to do with a machine, it describes a color.
 - Violation of industry best practices (string concatenation, naming conventions, etc.)

### Modifications

 - Defined an ```IMachine``` interface that contains all public methods.
 - Created an abstract ```Machine``` base classes to implement default behavior and values.
 - Defined a ```MachineType``` enumeration that provides a unique value for each machine type. NOTE: This enum also contains a "Boat" value that wasn't in the original code. This is primarily used for testing in this exercise and would not be included in production code.
 - Defined a ```MachineColor``` static class with named constants that behaves similar to Java's enum. My primary motivation for this was the logic to determine if a color is dark. That doesn't need to be in the Machine classes. It could've been added to a utility class, custom attributes, or an extension method, but it made the most sense to encapsulate that logic with each MachineColor. This new ```MachineColor``` class effectively works like an enum but allows for additional properties and behaviors within each color.
 - Created concrete classes for each type of machine (Bulldozer, Crane, etc.), and override each property as necessary. These classes are extremely lightweight, contain no logic, and are simple to understand and therefore modify in the future as needed.
 - Created a ```MachineFactory``` factory class that is used to instantiate the correct class from a given ```MachineType```. This is more of a utility than a necessity, as machine instances can still be instantiated on their own.
 - Created a FortCodeExercises.Tests project to test the modifications and ensure the results are as expected. There are multiple "systems under test" in the single test class. Typically I would create a separate test class per system but the scope of this exercise was small enough and a single test class is sufficient.
## Considerations and Exclusions

- I opted for the factory pattern to get instances of classes vs. something like a repository. Repositories suggest data that is coming from a data source (DB, file system, etc.), and here we just need to return class instances.
- I considered having a constructor that accepted the machine's name and type, but that would open up the possibility of mismatches. For example: ```var car = new Car(MachineType.Tractor, "boat");```. Those values should be read-only.
- I eliminated the logic in the ```getMaxSpeed()``` method. This was meaningless; there were no other dependencies and each machine should just return its max speed value.
- The project's target framework is .NET Core 3.1 which I left as-is. This could have been easily upgraded to the current version but I didn't have enough context on whether it made sense to do so. Perhaps the CI/CD pipelines or consumers of this are dependent on 3.1
