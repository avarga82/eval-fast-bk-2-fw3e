namespace FortCodeExercises.Exercise1
{
    public class Car : Machine
    {
        public override MachineType Type => MachineType.Car;
        public override MachineColor Color => MachineColor.Brown;
    }
}