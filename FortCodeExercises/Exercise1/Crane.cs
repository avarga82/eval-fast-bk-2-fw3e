namespace FortCodeExercises.Exercise1
{
    public class Crane : Machine
    {
        public override MachineType Type => MachineType.Crane;
        public override MachineColor Color => MachineColor.Blue;
        public override MachineColor TrimColor => Color.IsDark ? MachineColor.Black: MachineColor.White;
        public override int MaxSpeed => 75;
    }
}