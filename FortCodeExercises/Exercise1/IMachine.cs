namespace FortCodeExercises.Exercise1
{
    public interface IMachine
    {
        MachineType Type { get; }
        MachineColor Color { get; }   
        MachineColor TrimColor { get; }
        string Name { get; }
        string Description { get; }
        int MaxSpeed { get; }
    }
}