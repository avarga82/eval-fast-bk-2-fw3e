namespace FortCodeExercises.Exercise1
{
    public class MachineColor
    {
        public static readonly MachineColor White = new MachineColor("white", false);
        public static readonly MachineColor Blue = new MachineColor("blue", false);
        public static readonly MachineColor Red = new MachineColor("red", true);
        public static readonly MachineColor Brown = new MachineColor("brown", false);
        public static readonly MachineColor Yellow = new MachineColor("yellow", false);
        public static readonly MachineColor Green = new MachineColor("green", true);
        public static readonly MachineColor Beige = new MachineColor("beige", false);
        public static readonly MachineColor Black = new MachineColor("black", true);
        public static readonly MachineColor BabyBlue = new MachineColor("babyblue", false);
        public static readonly MachineColor Crimson = new MachineColor("crimson", true);
        public static readonly MachineColor Gold = new MachineColor("gold", false);
        public static readonly MachineColor Silver = new MachineColor("silver", false);

        public string Name { get; }
        public bool IsDark { get; }

        MachineColor(string name, bool isDark) => (Name, IsDark) = (name, isDark);
    }
}