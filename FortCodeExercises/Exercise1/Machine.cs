namespace FortCodeExercises.Exercise1
{
    public abstract class Machine : IMachine
    {
        private const int DefaultMaxSpeed = 70;
        
        public abstract MachineType Type { get; }
        public abstract MachineColor Color { get; }
        public virtual MachineColor TrimColor => null;
        public virtual string Name => Type.ToString().ToLower();
        public virtual string Description => $" {Color.Name} {Name} [{MaxSpeed}].";
        public virtual int MaxSpeed => DefaultMaxSpeed;

    }
}