namespace FortCodeExercises.Exercise1
{
    public enum MachineType
    {
        Bulldozer = 0,
        Crane,
        Tractor ,
        Truck,
        Car,
        Boat
    }
}