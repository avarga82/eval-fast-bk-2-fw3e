﻿using System;

namespace FortCodeExercises.Exercise1
{
    public static class MachineFactory
    {
        public static IMachine GetMachine(MachineType type) => type switch
        {
            MachineType.Bulldozer => new Bulldozer(),
            MachineType.Crane => new Crane(),
            MachineType.Tractor => new Tractor(),
            MachineType.Truck => new Truck(),
            MachineType.Car => new Car(),
            _ => throw new ArgumentOutOfRangeException(nameof(type), $"A machine could not be found for the specified type.")
        };
    }
}