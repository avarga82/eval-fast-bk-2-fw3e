namespace FortCodeExercises.Exercise1
{
    public class Truck : Machine
    {
        public override MachineType Type => MachineType.Truck;
        public override MachineColor Color => MachineColor.Yellow;
        public override MachineColor TrimColor => Color.IsDark ? MachineColor.Silver: null;
    }
}