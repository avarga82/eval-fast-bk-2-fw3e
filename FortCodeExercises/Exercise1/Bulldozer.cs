namespace FortCodeExercises.Exercise1
{
    public class Bulldozer : Machine
    {
        public override MachineType Type => MachineType.Bulldozer;
        public override MachineColor Color => MachineColor.Red;
        public override MachineColor TrimColor => null;
        public override int MaxSpeed => 80;
    }
}